DROP DATABASE IF EXISTS MainDataBase;
CREATE DATABASE MainDataBase;
USE MainDataBase;

CREATE TABLE FocusTests
(
   FocusID INT NOT NULL AUTO_INCREMENT,
   EndFocusValue FLOAT NOT NULL,
   EndFocusStep INT NOT NULL,
   FocusTime INT NOT NULL,
   FoundCore VARCHAR(5) NOT NULL,
   AverageFrameLatency INT NOT NULL,
   MaxFrameLatency INT NOT NULL,
   ImageName VARCHAR(255) NOT NULL,
   TimeStamp DATETIME NOT NULL,
   Battery FLOAT NOT NULL,
   Temperature FLOAT NOT NULL,
   PRIMARY KEY(FocusID)
);
CREATE TABLE AnalysisTests
(
	AnalysisID INT NOT NULL AUTO_INCREMENT,
	FocusID INT NOT NULL,
	TimeTaken INT NOT NULL,
	DefectCount INT NOT NULL,
	DefectPixelCount INT NOT NULL,
	Focused VARCHAR(5) NOT NULL,
	AnalysisPassed VARCHAR(5) NOT NULL,
	Centered VARCHAR(5) NOT NULL,
	PRIMARY KEY(AnalysisID)
);
CREATE TABLE StepInfo
(
   StepID INT NOT NULL AUTO_INCREMENT,
   FocusID INT NOT NULL,
   Step INT NOT NULL,
   TotalWaited INT NOT NULL,
   FocusValue float NOT NULL,
   PRIMARY KEY(StepID)
);