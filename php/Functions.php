<?php
function ConnectToDataBase()
{
	$dsn = 'mysql:host=localhost;port=3306;dbname=MainDataBase'; 
	$user = 'root';
	$password = 'RipleyODM2019!'; 
	$handle = new PDO($dsn, $user, $password); 
	$handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	return $handle; 
}
function UploadFile($file)
{
	$fileName=$file['name'];
	$fileTmpName=$file['tmp_name'];
	$fileSize=$file['size'];
	$fileError=$file['error'];
	$fileType=$file['type'];
	$fileExt = explode('.',$fileName);
	$fileActualExt= strtolower(end($fileExt));
	$allowed= array('png','jpeg','jpg');
	if(in_array(fileActualExt,allowed)
	{
		if($fileError===0)
		{
			if($fileSize<1000000)
			{
				$fileNameNew=uniqid('',true).".".$fileActualExt;
				$fileDestination='Uploads/'.$fileNameNew;
				move_uploaded_file($fileTmpName,$fileDestination);
			}
			else
			{
				echo "Your file is to large!";
			}
		}
		else
		{
			echo "there was a generic error uploading your file";
		}
	}
	else
	{
		echo "You cannot upload files of this type";
	}
}
function RunSQLStatement($Statement)
{
	$conn; 
	try{
		$conn = ConnectToDataBase(); 
	}catch(PDOException $ex){
		return "open error: ".$ex;
	}
	$proc_get_authors = $conn->prepare($Statement);
	try
	{
		$rs = $proc_get_authors->execute();
	}catch(PDOException $ex){
		$conn = null; 
		return "Bad sql. Sql statement".$Statement;
	}
	$rows = array(); 
	$conn = null;
	$retVal="";
	try{
	while($row = $proc_get_authors->fetch(PDO::FETCH_ASSOC)){
		$rows[] = $row; 
	}
	$retVal = json_encode($rows); 
	}
	catch(PDOException $ex)
	{
		$retVal = "Success";
	}
	$conn = null;
	return $retVal; 	
}

function AddFocusTest($EndFocusValue,$EndFocusStep,$FocusTime,$FoundCore,$AverageFrameLatency,$MaxFrameLatency,$ImageName,$Battery,$Temperature)
{
	$ReturnData=RunSQLStatement("INSERT INTO FocusTests (EndFocusValue,EndFocusStep,FocusTime,FoundCore,AverageFrameLatency,MaxFrameLatency,ImageName,TimeStamp,Battery,Temperature)  VALUES ('".$EndFocusValue."','".$EndFocusStep."','".$FocusTime."','".$FoundCore."','".$AverageFrameLatency."','".$MaxFrameLatency."','".$ImageName."',NOW(),'".$Battery."','".$Temperature."');");
	if( $ReturnData=="Success")
	{
		return RunSQLStatement("SELECT FocusID FROM FocusTests ORDER BY FocusID DESC LIMIT 0,1;");
	}
	else
	{
		return $ReturnData;
	}
}
function AddStep($FocusID,$Step,$TotalWaited,$FocusValue)
{
	$Statement="INSERT INTO StepInfo (FocusID,Step,TotalWaited,FocusValue) VALUES ('".$FocusID."','".$Step."','".$TotalWaited."','".$FocusValue."');";
	return RunSQLStatement($Statement);
}
function AddAnalysisTest($FocusID,$TimeTaken,$DefectCount,$DefectPixelCount,$Focused,$AnalysisPassed,$Centered)
{
	$Statement="INSERT INTO AnalysisTests (FocusID,TimeTaken,DefectCount,DefectPixelCount,Focused,AnalysisPassed,Centered) VALUES ('".$FocusID."','".$TimeTaken."','".$DefectCount."','".$DefectPixelCount."','".$Focused."','".$AnalysisPassed."','".$Centered."');";
	return RunSQLStatement($Statement);
}
?>